var mymap = L.map('main_map').setView([4.7070615,-74.1212116], 15);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 15,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiamhvbmZpZXJybzQ4IiwiYSI6ImNrcmNidnhueDUxNjYycG84Yjl3YWlrb3MifQ.-z10I0rcfo7K_mYWCcwI4A'
}).addTo(mymap);

var IconBici = L.icon({
    iconUrl: '/images/icon-bicicleta.png',
    iconSize: [36, 36]
    });

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        //console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {icon: IconBici}, {title: bici.id}).addTo(mymap);
        });
    }
})