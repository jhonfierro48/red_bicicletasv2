var mongoose = require('mongoose');
var Bicicleta = require ('../../models/bicicleta');
var Usuario = require ('../../models/usuario');
var Reserva = require ('../../models/reserva');


describe('Testing Usuarios', function(){
    beforeEach(function(done) { 
        var mongoDB = 'mongodb://localhost/testdb';
        //mongoose.connect(mongoDB, {userNewUrlParser: true });
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('Nosotros estamos conectados a la base de datos');
            done();
        });
    });



    describe('Cuando un usuario reserva una bici', () => {
        it('debe existir la reserva', (done) =>{
            const usuario = new Usuario({nombre: 'Jhon Alexander'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                
                });
            });
        });
    });

    
    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, sucess){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, sucess){
                    if (err) console.log(err);
                    //mongoose.disconnect(err);
                    done(); 
                });
            });
        });
    });

});
